<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class JobPositions extends Model
{
    use ElasticquentTrait;

    protected $fillable = [
        'title',
        'category',
        'min_age',
        'max_age',
        'education',
        'gender',
        'salary',
        'location',
    ];

    const UPDATED_AT = null;
}
