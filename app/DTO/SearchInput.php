<?php

namespace App\DTO;

class SearchInput
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var int
     */
    private $minAge;

    /**
     * @var int
     */
    private $maxAge;

    /**
     * @var string
     */
    private $education;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $sort;

    /**
     * @var string
     */
    private $minCreatedAt;

    /**
     * @var string
     */
    private $maxCreatedAt;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): SearchInput
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     */
    public function setCategory(?string $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     */
    public function setGender(?string $gender): SearchInput
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinAge(): ?int
    {
        return $this->minAge;
    }

    /**
     * @param int|null $minAge
     */
    public function setMinAge(?int $minAge): SearchInput
    {
        $this->minAge = $minAge;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }

    /**
     * @param int|null $maxAge
     */
    public function setMaxAge(?int $maxAge): SearchInput
    {
        $this->maxAge = $maxAge;

        return $this;
    }

    /**
     * @return string
     */
    public function getEducation(): ?string
    {
        return $this->education;
    }

    /**
     * @param string|null $education
     *
     * @return $this
     */
    public function setEducation(?string $education): SearchInput
    {
        $this->education = $education;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     *
     * @return $this
     */
    public function setLocation(?string $location): SearchInput
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSort(): ?string
    {
        return $this->sort;
    }

    /**
     * @param string|null $sort
     *
     * @return $this
     */
    public function setSort(?string $sort): SearchInput
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMinCreatedAt(): ?string
    {
        return $this->minCreatedAt;
    }

    /**
     * @param string|null $minCreatedAt
     */
    public function setMinCreatedAt(?string $minCreatedAt): SearchInput
    {
        $this->minCreatedAt = $minCreatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaxCreatedAt(): ?string
    {
        return $this->maxCreatedAt;
    }

    /**
     * @param string $maxCreatedAt
     */
    public function setMaxCreatedAt(?string $maxCreatedAt): SearchInput
    {
        $this->maxCreatedAt = $maxCreatedAt;

        return $this;
    }
}
