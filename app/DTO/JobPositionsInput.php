<?php

namespace App\DTO;

class JobPositionsInput
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $category;

    /**
     * @var int
     */
    private $minAge;

    /**
     * @var int
     */
    private $maxAge;

    /**
     * @var string
     */
    private $education;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var float
     */
    private $salary;

    /**
     * @var string
     */
    private $location;

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     *
     * @return $this
     */
    public function setCategory(?string $category): JobPositionsInput
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @param string|null $education
     *
     * @return $this
     */
    public function setEducation(?string $education): JobPositionsInput
    {
        $this->education = $education;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     *
     * @return $this
     */
    public function setGender(?string $gender): JobPositionsInput
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxAge()
    {
        return $this->maxAge;
    }

    /**
     * @param int|null $maxAge
     *
     * @return $this
     */
    public function setMaxAge(?int $maxAge): JobPositionsInput
    {
        $this->maxAge = $maxAge;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinAge()
    {
        return $this->minAge;
    }

    /**
     * @param int|null $minAge
     *
     * @return $this
     */
    public function setMinAge(?int $minAge): JobPositionsInput
    {
        $this->minAge = $minAge;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     *
     * @return $this
     */
    public function setLocation(?string $location): JobPositionsInput
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param int|null $salary
     *
     * @return $this
     */
    public function setSalary(?int $salary): JobPositionsInput
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return $this
     */
    public function setTitle(?string $title): JobPositionsInput
    {
        $this->title = $title;

        return $this;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'category' => $this->category,
            'min_age' => $this->minAge,
            'max_age' => $this->maxAge,
            'education' => $this->education,
            'gender' => $this->gender,
            'salary' => $this->salary,
            'location' => $this->location,
        ];
    }
}
