<?php

namespace App\Services;

use App\DTO\JobPositionsInput;
use App\Repositories\Contract\RepositoryInterface;

class JobPositionsService
{
    /**
     * @var RepositoryInterface
     */
    private $jobPositionsRepository;

    public function __construct(RepositoryInterface $jobPositionsRepository)
    {
        $this->jobPositionsRepository = $jobPositionsRepository;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function find(int $id): array
    {
        $jobPosition = $this->jobPositionsRepository->find($id);

        return $jobPosition ? $jobPosition->toArray() : [];
    }

    /**
     * @param JobPositionsInput $jobPositionsInput
     */
    public function create(JobPositionsInput $jobPositionsInput)
    {
        $jobPosition = $this->jobPositionsRepository->create($jobPositionsInput->toArray());

        $jobPosition->addToIndex();
    }

    /**
     * @param int $id
     *
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $this->jobPositionsRepository->update($id, $data);

        $this->jobPositionsRepository->find($id)->updateIndex();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->jobPositionsRepository->delete($id);
    }
}
