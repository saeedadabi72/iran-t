<?php

namespace App\Services;

class Query
{
    /**
     * @var array
     */
    private $filter;

    /**
     * @var array
     */
    private $terms = [];

    /**
     * @var array
     */
    private $ranges = [];

    /**
     * @var array
     */
    private $sort = [];

    public function setFilter($filter)
    {
        $this->filter[] = [
            'match' => [
                $filter['field'] => [
                    'query' => $filter['value']
                ]
            ]
        ];

        return $this;
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function setTerm(array $term)
    {
        $this->terms[] = [
            'term' => $term,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getTerms()
    {
        return $this->terms;
    }

    public function setRange(array $range)
    {
        $this->ranges[] = ['range' => $range];

        return $this;
    }

    public function getRanges()
    {
        return $this->ranges;
    }

    public function setSort(array $sort)
    {
        $this->sort[] = $sort;

        return $this;
    }

    public function getSort()
    {
        return $this->sort;
    }
}
