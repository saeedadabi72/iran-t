<?php

namespace App\Services\Filters;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;

class MaxAgeFilter implements FilterInterface
{
    public function support(SearchInput $input): bool
    {
        if ($input->getMaxAge()) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        $query->setRange(['max_age' => ['lte' => $input->getMaxAge()]]);
    }
}


