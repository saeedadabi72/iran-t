<?php

namespace App\Services\Filters;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;

class MinAgeFilter implements FilterInterface
{
    public function support(SearchInput $input): bool
    {
        if ($input->getMinAge()) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        $query->setRange(['min_age' => ['gte' => $input->getMinAge()]]);
    }
}


