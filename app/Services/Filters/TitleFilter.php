<?php

namespace App\Services\Filters;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;

class TitleFilter implements FilterInterface
{
    public function support(SearchInput $input): bool
    {
        if ($input->getTitle()) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        $query->setFilter(['field' => 'title', 'value' => $input->getTitle()]);
    }
}


