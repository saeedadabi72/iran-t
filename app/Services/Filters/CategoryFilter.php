<?php

namespace App\Services\Filters;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;

class CategoryFilter implements FilterInterface
{
    public function support(SearchInput $input): bool
    {
        if ($input->getCategory()) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        $query->setTerm(['category' => $input->getCategory()]);
    }
}


