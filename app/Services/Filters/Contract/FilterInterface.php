<?php

namespace App\Services\Filters\Contract;

use App\DTO\SearchInput;
use App\Services\Query;

interface FilterInterface
{
    public function support(SearchInput $input): bool;

    public function manipulate(Query $query, SearchInput $input): void;
}
