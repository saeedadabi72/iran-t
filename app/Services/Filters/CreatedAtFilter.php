<?php

namespace App\Services\Filters;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;

class CreatedAtFilter implements FilterInterface
{
    public function support(SearchInput $input): bool
    {
        if ($input->getMinCreatedAt() || $input->getMaxCreatedAt()) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        if ($input->getMinCreatedAt()) {
            $query->setRange(['created_at' => ['gte' => $input->getMinCreatedAt()]]);
        }

        if ($input->getMaxCreatedAt()) {
            $query->setRange(['created_at' => ['lte' => $input->getMaxCreatedAt()]]);
        }
    }
}


