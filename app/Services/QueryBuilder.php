<?php

namespace App\Services;

class QueryBuilder
{
    /**
     * @var array
     */
    private $lastQuery;

    /**
     * @var Query
     */
    private $query;

    /**
     * @var array
     */
    private $filter;

    /**
     * @var array
     */
    private $must;

    /**
     * @var \stdClass
     */
    private $bool;

    /**
     * @var array
     */
    private $sort;

    public function __construct(Query $query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function create()
    {
        $this->addMust()
            ->addFilter()
            ->addBool()
            ->addSort();

        if ($this->bool) {
            $this->lastQuery['body']['query']['bool'] = $this->bool;
        }

        if ($this->sort) {
            $this->lastQuery['body']['sort'] = $this->sort;
        }

        return $this->lastQuery;
    }

    /**
     * @return $this
     */
    private function addFilter()
    {
        $this->filter['filter'] = $this->query->getFilter();

        return $this;
    }

    /**
     * @return $this
     */
    private function addMust()
    {
        foreach ($this->query->getTerms() as $term) {
            $this->must['must'][] = $term;
        }

        foreach ($this->query->getRanges() as $range) {
            $this->must['must'][] = $range;
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function addBool()
    {
        $this->bool = new \stdClass();

        if (!empty($this->must['must'])) {
            $this->bool->must = $this->must['must'];
        }

        if (!empty($this->filter['filter'])) {
            $this->bool->filter = $this->filter['filter'];
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function addSort()
    {
        $this->sort = $this->query->getSort();

        return $this;
    }
}
