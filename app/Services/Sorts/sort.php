<?php

namespace App\Services\Sorts;

use App\DTO\SearchInput;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Query;
use App\Services\Sorts\Contract\SortInterface;

class sort implements FilterInterface
{
    private $sort;

    public function support(SearchInput $input): bool
    {
        $this->sort = explode("_", $input->getSort());

        if ($input->getSort() && in_array($this->sort[1], ['asc','desc'])) {
            return true;
        }

        return false;
    }

    public function manipulate(Query $query, SearchInput $input): void
    {
        $query->setSort([$this->sort[0] => ["order" => $this->sort[1]]]);
    }
}


