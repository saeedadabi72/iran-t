<?php

namespace App\Services;

use App\DTO\SearchInput;
use App\JobPositions;
use App\Services\Filters\Contract\FilterInterface;

class SearchService
{
    /**
     * @var FilterInterface[]
     */
    private $filters;

    public function __construct(FilterInterface ...$filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param SearchInput $searchInput
     *
     * @return array
     */
    public function search(SearchInput $searchInput)
    {
        $query = new Query();
        foreach ($this->filters as $filter)
        {
            if ($filter->support($searchInput)) {
                $filter->manipulate($query, $searchInput);
            }
        }

        $queryBuilder = new QueryBuilder($query);
        $query = $queryBuilder->create();

        $jobPositions = JobPositions::complexSearch($query);

        $result = [];
        foreach ($jobPositions->getHits()['hits'] as $hit) {
            $result[] = $hit['_source'];
        }

        return $result;
    }
}
