<?php

namespace App\Repositories;

use App\JobPositions;
use App\Repositories\Contract\RepositoryInterface;
use Illuminate\Queue\Jobs\Job;

class JobPositionsRepository implements RepositoryInterface
{
    public function find(int $id)
    {
        return JobPositions::find($id);
    }

    public function getAll()
    {
        return JobPositions::all();
    }

    public function create(array $data)
    {
        return JobPositions::create($data);
    }

    public function update(int $id, array $data)
    {
//        print_r($this->find($id));die;
        return JobPositions::where('id', $id)->update($data);
    }

    public function delete(int $id)
    {
        JobPositions::destroy($id);
    }
}
