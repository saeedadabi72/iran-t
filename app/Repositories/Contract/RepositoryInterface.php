<?php

namespace App\Repositories\Contract;

interface RepositoryInterface {

    public function find(int $id);

    public function getAll();

    public function create(array $data);

    public function update(int $id, array $data);

    public function delete(int $id);

}
