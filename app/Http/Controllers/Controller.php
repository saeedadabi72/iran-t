<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array $data
     * @param int   $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function json(array $data = [], int $statusCode = 200)
    {
        return response()->json(
            [
                'status' => true,
                'data'   => $data,
            ], $statusCode
        );
    }

    /**
     * @param array $data
     * @param int   $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function failed(array $data = [], int $statusCode = 400)
    {
        return response()->json(
            [
                'status' => false,
                'data'   => $data,
            ], $statusCode
        );
    }
}
