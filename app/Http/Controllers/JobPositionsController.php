<?php

namespace App\Http\Controllers;

use App\DTO\JobPositionsInput;
use App\Services\JobPositionsService;
use Illuminate\Http\Request;

class JobPositionsController extends Controller
{
    /**
     * @var JobPositionsService
     */
    private $jobPositionsService;

    public function __construct(JobPositionsService $jobPositionsService)
    {
        $this->jobPositionsService = $jobPositionsService;
    }

    public function create(Request $request)
    {
        $jopPositionInput = new JobPositionsInput();
        $jopPositionInput->setCategory($request->category)->setEducation($request->education)
            ->setGender($request->gender)->setMaxAge($request->maxAge)
            ->setMinAge($request->minAge)->setSalary($request->salary)
            ->setTitle($request->title)->setLocation($request->location);

        try {
            $this->jobPositionsService->create($jopPositionInput);
        } catch (\Exception $e) {
            return $this->failed();
        }

        return $this->json();
    }

    public function read($id)
    {
        try {
            $jobPosition = $this->jobPositionsService->find($id);
        } catch (\Exception $e) {
            return $this->failed();
        }

        return $this->json($jobPosition);
    }

    public function update($id, Request $request)
    {
        try {
            $this->jobPositionsService->update($id, $request->all());
        } catch (\Exception $e) {
            return $this->failed();
        }

        return $this->json();
    }

    public function delete($id)
    {
        try {
            $this->jobPositionsService->delete($id);
        } catch (\Exception $e) {
            return $this->failed();
        }

        return $this->json();
    }
}
