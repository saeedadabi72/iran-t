<?php

namespace App\Http\Controllers;

use App\DTO\JobPositionsInput;
use App\DTO\SearchInput;
use App\Services\JobPositionsService;
use App\Services\SearchService;
use Illuminate\Http\Request;
use PHPUnit\Framework\MockObject\Exception;

class SearchController extends Controller
{
    /**
     * @var SearchService
     */
    private $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function search(Request $request)
    {
        $searchInput = new SearchInput();
        $searchInput->setTitle($request->title)
            ->setCategory($request->category)
            ->setMinAge($request->minAge)
            ->setMaxAge($request->maxAge)
            ->setGender($request->gender)
            ->setEducation($request->education)
            ->setLocation($request->location)
            ->setMinCreatedAt($request->minCreatedAt)
            ->setMaxCreatedAt($request->maxCreatedAt)
            ->setSort($request->sort);

        $jobPositions = $this->searchService->search($searchInput);

        return $this->json($jobPositions);
    }
}
