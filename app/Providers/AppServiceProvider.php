<?php

namespace App\Providers;

use App\Services\Filters\CategoryFilter;
use App\Services\Filters\Contract\FilterInterface;
use App\Services\Filters\CreatedAtFilter;
use App\Services\Filters\EducationFilter;
use App\Services\Filters\GenderFilter;
use App\Services\Filters\LocationFilter;
use App\Services\Filters\MaxAgeFilter;
use App\Services\Filters\MinAgeFilter;
use App\Services\Filters\TitleFilter;
use App\Services\SearchService;
use App\Services\Sorts\sort;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contract\RepositoryInterface',
            'App\Repositories\JobPositionsRepository'
        );

        $this->app->when(SearchService::class)
            ->needs(FilterInterface::class)
            ->give(function ($app) {
                return [
                    $app->make(CategoryFilter::class),
                    $app->make(TitleFilter::class),
                    $app->make(GenderFilter::class),
                    $app->make(MinAgeFilter::class),
                    $app->make(MaxAgeFilter::class),
                    $app->make(EducationFilter::class),
                    $app->make(LocationFilter::class),
                    $app->make(CreatedAtFilter::class),
                    $app->make(sort::class),
                ];
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
