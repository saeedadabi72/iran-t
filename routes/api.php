<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/', function (Request $request) {
    \App\JobPositions::createIndex();
});


Route::post('/jobPosition', 'JobPositionsController@create');
Route::get('/jobPosition/{id}', 'JobPositionsController@read');
Route::put('/jobPosition/{id}', 'JobPositionsController@update');
Route::delete('/jobPosition/{id}', 'JobPositionsController@delete');
Route::get('/search', 'SearchController@search');
