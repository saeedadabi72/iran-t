<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_positions', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->string("category");
            $table->integer("min_age")->nullable();
            $table->integer("max_age")->nullable();
            $table->enum("education",["bsc","md","phd","diploma"]);
            $table->enum("gender",["male","female"])->nullable();
            $table->integer("salary")->nullable();
            $table->string("location")->nullable();
            $table->timestamp('created_at');
            $table->timestamp('expired_at')->useCurrent();
            $table->timestamp('lived_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_positions');
    }
}
