<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
;
use Faker\Generator as Faker;

$factory->define(App\JobPositions::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle(),
        'category' => $faker->randomElement(['it','designer','accounting','health care','laboratory','lawyer']),
        'min_age' => $faker->numberBetween(18, 22),
        'max_age' => $faker->numberBetween(23, 40),
        'education' => $faker->randomElement(['bsc','md','phd','diploma']),
        'gender' => $faker->randomElement(['male','female']),
        'salary' => $faker->numberBetween(10000000, 4000000),
        'location' => $faker->randomElement(['tehran','isfahan','iran']),
    ];
});
