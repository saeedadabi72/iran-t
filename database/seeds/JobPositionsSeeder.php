<?php

use App\JobPositions;
use Illuminate\Database\Seeder;

class JobPositionsSeeder extends Seeder
{
    const CHUNK_SIZE = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobPositions::truncate();
        JobPositions::deleteIndex();
        JobPositions::createIndex();

        $jobPositions = factory(JobPositions::class, 500)->make();
        $chunks = $jobPositions->chunk(self::CHUNK_SIZE);

        $chunks->each(function ($chunk) {
            JobPositions::insert($chunk->toArray());
            $chunk->addToIndex();
        });
    }
}
